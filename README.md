# NEF stats

A little utility written for me, to make some statistics on my forever-growing collection of photos in NEF format.


## nefstatsanalyzer

Once you run `nef-stats` and you got yourself a nice csv file with some juicy
informations on your photos, call `nefstatsanalyzer` on that csv, easy as:

    ./nefstatsanalyzer data_exported_with_nef-stats.csv


## Dependencies

To use `nefstatsanalyzer` you need three python libraries:
- `matplotlib`
- `pandas`
- `seaborn`


## Tests

To run the tests simply run:

    python3 -m unittest
